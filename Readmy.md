##Система подсчёта скоринга

###Установка
Склонировать репозиторий.\
Установить зависимости:
```
composer install
```

Отредактировать доступ к базе в .env

Создать базы:
```
./bin/console doctrine:database:create
./bin/console --env=test doctrine:database:create
```

Загрузить фикстуры:
```
./bin/console doctrine:fixtures:load
```

Запустить рассчёт скоринга:
```
./bin/console app:calculate-scoring
```

Админка доступна по адресу /admin \
email: admin@mail.com \
пароль: password

###Тесты

Загрузить фикстуры:
```
./bin/console --env=test doctrine:fixtures:load
```

Запуск тестов:
```
./bin/phpunit
```
