<?php


namespace App\Command;



use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Service\ScoringService;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScoringCommand extends Command
{

    protected static $defaultName = 'app:calculate-scoring';
    private  $scoringService;
    private  $em;

    public function __construct(ScoringService $scoringService, EntityManagerInterface $em)
    {
        $this->scoringService = $scoringService;
        $this->em = $em;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clientId = $input->getArgument('client_id');

        if ($clientId) {
            $client =$this->em->getRepository(Client::class)->find($clientId);

            if (!$client) {
                $output->writeln("Клиент с id {$clientId} не найден");
                return Command::FAILURE;
            }

            $clients = [$client];
        } else {
            $clients = $this->em->getRepository(Client::class)->findAll();
        }


        foreach ($clients as $client) {
            $scoring = $this->scoringService->setClient($client);
            $client->setScoring($scoring->getTotalScoring());
            $this->em->persist($client);
            $processingString = $client->getProcessingConsent() ? 'Есть' : 'Нет';

            $output->writeLn(<<<TEXT
Клиент: {$client->getName()}.
Email: {$client->getEmail()} (Скоринг: {$scoring->getEmailScoring()}).
Телефон: {$client->getPhone()} (Скоринг: {$scoring->getMobileScoring()}).
Образование: {$client->getEducation()->getName()} (Скоринг: {$scoring->getEducationScoring()}).
Разрешение на обработку персональных данных: {$processingString} (Скоринг: {$scoring->getProcessingScoring()}).
Скоринговый бал клиента: {$scoring->getTotalScoring()}.
================================
TEXT
            );
        }
        $this->em->flush();


        return Command::SUCCESS;
    }

    protected function configure()
    {
        $this->addArgument('client_id', InputArgument::OPTIONAL, 'Client id');
    }

}