<?php

namespace App\Repository;

use App\Entity\MobileOperator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @method MobileOperator|null find($id, $lockMode = null, $lockVersion = null)
 * @method MobileOperator|null findOneBy(array $criteria, array $orderBy = null)
 * @method MobileOperator[]    findAll()
 * @method MobileOperator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MobileOperatorRepository extends ServiceEntityRepository
{
    private $container;


    public function __construct(ManagerRegistry $registry, ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($registry, MobileOperator::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(MobileOperator $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(MobileOperator $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }


    public function findByPhone($phone): ?MobileOperator
    {
        $code = substr($phone, 0, 3);

        $result = $this->createQueryBuilder('e')
            ->andWhere('e.codes LIKE :code')
            ->setParameter('code', "%{$code}%")
            ->getQuery()
            ->getOneOrNullResult();

        if (!$result) {
            $result = new MobileOperator();
            $result->setScoringValue($this->container->getParameter('app.default_phone_scoring'));
        }

        return $result;
    }

    /*
    public function findOneBySomeField($value): ?MobileOperator
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
