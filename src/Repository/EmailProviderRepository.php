<?php

namespace App\Repository;

use App\Entity\EmailProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @method EmailProvider|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailProvider|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailProvider[]    findAll()
 * @method EmailProvider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailProviderRepository extends ServiceEntityRepository
{
    private $container;


    public function __construct(ManagerRegistry $registry, ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($registry, EmailProvider::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(EmailProvider $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(EmailProvider $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return EmailProvider[] Returns an array of EmailProvider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findByEmail($email): ?EmailProvider
    {
        $domainArray =explode('@', $email);
        $domain = array_pop($domainArray);

        $result = $this->createQueryBuilder('e')
            ->andWhere('e.domains LIKE :email')
            ->setParameter('email', "%{$domain}%")
            ->getQuery()
            ->getOneOrNullResult();

        if (!$result) {
            $result = new EmailProvider();
            $result->setScoringValue($this->container->getParameter('app.default_email_scoring'));
        }

        return $result;
    }
}
