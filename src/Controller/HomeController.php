<?php


namespace App\Controller;
use App\Entity\Client;
use App\Entity\Education;
use App\Form\Type\ClientType;
use App\Service\ScoringService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends  AbstractController
{

    /**
     * @Route("/")
     * @return Response
     */
    public function index(EntityManagerInterface $entityManager, Request $request, ScoringService $scoringService): Response
    {
        $educations = $entityManager->getRepository(Education::class)->findAll();

        $educationChoices = [];
        foreach ($educations as $education) {
            $educationChoices[$education->getName()] = $education;
        }

        $client = new Client();
        $form = $this->createForm(ClientType::class, $client, ['educations' => $educationChoices]);
        $form->getData();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $client->setScoring($scoringService->setClient($client)->getTotalScoring());
            $entityManager->persist($client);
            $entityManager->flush();

            $this->addFlash('success', "Вы успешно зарегестрированы");
        }


        return $this->renderForm('home/index.html.twig', ['form' => $form]);
    }

}