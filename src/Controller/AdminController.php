<?php


namespace App\Controller;


use App\Entity\Client;
use App\Entity\Education;
use App\Form\Type\ClientType;
use App\Service\ScoringService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    private $em;
    private $request;
    private $scoringService;

    public function __construct(EntityManagerInterface $em, ScoringService $scoringService)
    {
        $this->em = $em;
        $this->scoringService = $scoringService;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(PaginatorInterface $paginator,  Request $request)
    {
        $clientsQuery = $this->em->getRepository(Client::class)->createQueryBuilder('c')
            ->orderBy('c.id', 'ASC')
            ->join('c.education', 'e')
            ->where('e.id = c.education')
            ->getQuery();

        $pagination = $paginator->paginate($clientsQuery, $request->query->getInt('page', 1), 20);

        return $this->render('admin/index.html.twig', ['pagination' => $pagination]);

    }

    /**
     * @Route("/admin/edit/{client}", name="edit")
     */
    public function edit(Client $client,  Request $request)
    {
        $form = $this->getForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveClient($form);

            $this->addFlash('success', "Клиент  {$client->getName()}  успешно сохранён");
            return $this->redirectToRoute('admin');
        }

        return $this->renderForm('admin/form.html.twig', ['form' => $form, 'client' => $client]);
    }


    /**
     * @Route("/admin/create", name="create")
     */
    public function create(Request $request)
    {
        $client = new Client();
        $form = $this->getForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveClient($form);

            $this->addFlash('success', "Клиент  {$client->getName()}  успешно добавлен");
            return $this->redirectToRoute('admin');
        }

        return $this->renderForm('admin/form.html.twig', ['form' => $form, 'client' => $client]);
    }

    /**
     * @Route("/admin/delete/{client}", name="delete")
     */
    public function delete(Client $client)
    {
        $this->em->remove($client);
        $this->em->flush();

        $this->addFlash('success', "Клиент {$client->getName()} удалён");
        return $this->redirectToRoute('admin');

    }

    /**
     * @param FormInterface $form
     */
    private function saveClient(FormInterface $form): void
    {
        $client = $form->getData();

        $client->setScoring($this->scoringService->setClient($client)->getTotalScoring());

        $this->em->persist($client);
        $this->em->flush();
    }

    /**
     * @param Client $client
     * @return mixed|\Symfony\Component\Form\FormInterface
     */
    private function getForm(Client $client)
    {
        $educations = $this->em->getRepository(Education::class)->findAll();

        $educationChoices = [];
        foreach ($educations as $education) {
            $educationChoices[$education->getName()] = $education;
        }

        $form = $this->createForm(ClientType::class, $client, ['educations' => $educationChoices, 'edit' => true]);
        return $form;
    }



}