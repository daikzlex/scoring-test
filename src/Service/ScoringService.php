<?php


namespace App\Service;


use App\Entity\Client;
use App\Entity\EmailProvider;
use App\Entity\MobileOperator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ScoringService
{
    private $entityManager;

    private $container;
    private $client;


    private $totalScoring = 0;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->entityManager = $em;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    public function getMobileScoring()
    {
        $mobileOperator = $this->entityManager
            ->getRepository(MobileOperator::class)
            ->findByPhone($this->client->getPhone());


        return $mobileOperator->getScoringValue();
    }

    public function getEmailScoring()
    {

        $emailProvider = $this->entityManager
            ->getRepository(EmailProvider::class)
            ->findByEmail($this->client->getEmail());


        return $emailProvider->getScoringValue();
    }

    public function getProcessingScoring()
    {
        $processingScoring = 0;
        if ($this->client->getProcessingConsent()) {
            $processingScoring = $this->container->getParameter('app.processing_consent_scoring');
        }
        return $processingScoring;
    }

    public function getEducationScoring()
    {
        return $this->client->getEducation()->getScoringValue();
    }

    public function getTotalScoring()
    {
        return  $this->getEducationScoring()
            + $this->getMobileScoring()
            + $this->getEmailScoring()
            + $this->getProcessingScoring();
    }

}