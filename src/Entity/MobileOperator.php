<?php

namespace App\Entity;

use App\Repository\MobileOperatorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MobileOperatorRepository::class)
 */
class MobileOperator
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $codes;

    /**
     * @ORM\Column(type="smallint")
     */
    private $scoring_value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodes(): ?string
    {
        return $this->codes;
    }

    public function setCodes(string $codes): self
    {
        $this->codes = $codes;

        return $this;
    }

    public function getScoringValue(): ?int
    {
        return $this->scoring_value;
    }

    public function setScoringValue(int $scoring_value): self
    {
        $this->scoring_value = $scoring_value;

        return $this;
    }
}
