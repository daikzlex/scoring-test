<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use App\Repository\EmailProviderRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Positive
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Education", inversedBy="clients")
     */
    private $education;

    /**
     * @ORM\Column(type="boolean")
     */
    private $processing_consent;

    /**
     *  @ORM\Column(type="integer")
     */
    private $scoring;

    public function getScoring(): ?int
    {
        return $this->scoring;
    }

    public function setScoring(int $scoring): self
    {
        $this->scoring = $scoring;
        return $this;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEducation(): ?Education
    {
        return $this->education;
    }

    public function setEducation(Education $education): self
    {
        $this->education = $education;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


    public function getProcessingConsent(): ?bool
    {
        return $this->processing_consent;
    }

    public function setProcessingConsent(bool $processing_consent): self
    {
        $this->processing_consent = $processing_consent;

        return $this;
    }

}
