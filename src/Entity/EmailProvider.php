<?php

namespace App\Entity;

use App\Repository\EmailProviderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmailProviderRepository::class)
 */
class EmailProvider
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $domains;

    /**
     * @ORM\Column(type="smallint")
     */
    private $scoring_value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getScoringValue(): ?int
    {
        return $this->scoring_value;
    }

    public function setScoringValue(int $scoring_value): self
    {
        $this->scoring_value = $scoring_value;

        return $this;
    }

    public function getDomains(): ?string
    {
        return $this->domains;
    }

    public function setDomains(string $domains): self
    {
        $this->domains = $domains;
        return $this;

    }


}
