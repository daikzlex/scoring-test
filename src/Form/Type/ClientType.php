<?php


namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', TextType::class, ['label' => 'Имя: ', 'required' => true])
            ->add('phone', TelType::class, ['label' => 'Телефон: +7', 'required' => true])
            ->add('email', TextType::class, ['label' => 'Email: ', 'required' => true])
            ->add('education', ChoiceType::class, ['choices'  => $options['educations'], 'label' => 'Образование: ', 'required' => true])
            ->add('processing_consent', CheckboxType::class, [
                'label'    => 'Разрешить обработку персональных данных',
                'required' => false,
            ])
        ->add('submit', SubmitType::class, ['label' => ($options['edit']) ? 'Сохранить' : 'Регистрация']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'educations' => [],
                'edit' => false
            )
        //'data_class' => 'DataBaseBundle\Entity\Cities'
        );
        }

}