<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Education;
use App\Entity\EmailProvider;
use App\Entity\MobileOperator;
use App\Repository\EducationRepository;
use App\Service\ScoringService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private  $operators = [];
    private  $emailProviders = [
        'gmail' => ['hosts' => ['gmail.com'], 'scoring' => 10],
        'yandex' => ['hosts' => ['yandex.ru'], 'scoring' => 8],
        'mail' => ['hosts' => ['mail.ru', 'internet.ru', 'list.ru', 'bk.ru', 'inbox.ru', 'mail.ua'], 'scoring' => 6]
    ];

    private   $educations = [
        'Высшее образование' => 15,
        'Специальное образование' => 10,
        'Среднее образование' => 5
    ];

    private $educationsModels = [];



    public function __construct()
    {
        $this->operators = [  'Мегафон' => [
            'codes' => range(920, 929),
            'scoring' => 10
        ],
            'Билайн' => [
                'codes' => array_merge([903, 905, 906, 909, 951, 953], range(960, 968)),
                'scoring' => 5
            ],
            'Мтс' => [
                'codes' => array_merge(range(910, 919), range(980, 989)),
                'scoring' => 3
            ]];



    }

    public function load(ObjectManager $manager): void
    {
        $this->setEducations($manager);
        $this->setOperators($manager);
        $this->setEmailProviders($manager);

        $manager->flush();
        $emailDomains = ['gmail.com', 'other.com', 'mail.ru', 'yandex.ru'];

        for ($i = 1; $i <= 50; ++$i) {
            $client = new Client();
            $client->setName("test {$i}");
            $client->setEmail("test_{$i}@{$emailDomains[array_rand($emailDomains)]}");
            $client->setPhone(rand(900, 990) . rand(900, 990) . rand(10, 99) . rand(10,99));
            $client->setEducation($this->educationsModels[array_rand($this->educationsModels)]);
            $client->setProcessingConsent(rand(0, 2));
            $client->setScoring(0);

            $manager->persist($client);
        }

        $manager->flush();


    }

    /**
     * @param ObjectManager $manager
     */
    private function setEducations(ObjectManager $manager): void
    {


        foreach ($this->educations as $key => $value) {
            $education = new Education();
            $education->setName($key);
            $education->setScoringValue($value);

            $this->educationsModels[] = $education;

            $manager->persist($education);
        }
    }

    /**
     * @param ObjectManager $manager
     */
    private function setOperators(ObjectManager $manager): void
    {


        foreach ($this->operators as $key => $value) {
            $operator = new MobileOperator();
            $operator->setName($key);
            $operator->setCodes(implode(PHP_EOL, $value['codes']));
            $operator->setScoringValue($value['scoring']);

            $manager->persist($operator);
        }
    }

    /**
     * @param ObjectManager $manager
     */
    private function setEmailProviders(ObjectManager $manager): void
    {


        foreach ($this->emailProviders as $key => $value) {
            $emailProvider = new EmailProvider();
            $emailProvider->setName($key);
            $emailProvider->setDomains(implode(PHP_EOL, $value['hosts']));
            $emailProvider->setScoringValue($value['scoring']);

            $manager->persist($emailProvider);
        }
    }
}
