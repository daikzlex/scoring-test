<?php


namespace App\Tests\Service;


use App\Entity\Client;
use App\Entity\Education;
use App\Service\ScoringService;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ScoringServiceTest extends KernelTestCase
{


    /**
     * @var ScoringService
     */
    private $scoringService;

    protected function setUp(): void
    {
        $container = self::getContainer();
        $this->scoringService = $container->get(ScoringService::class);
    }

    /**
     * @dataProvider phonesProvider
     */
    public function testPhones($phone, $excepted)
    {
        $client = new Client();
        $client->setPhone($phone);

        $this->assertEquals(
            $this->scoringService
                ->setClient($client)
                ->getMobileScoring(),
            $excepted
        );
    }

    public function phonesProvider(): array
    {
        return [
            'Билайн' => ['9609999999', 5],
            'МТС' => ['9109999999', 3],
            'Мегафон' => ['9209999999', 10],
            'Прочее' => ['9009999999', 1]
        ];
    }

    /**
     * @dataProvider emailsProvider
     */
    public function testEmails($email, $excepted)
    {
        $client = new Client();
        $client->setEmail($email);

        $this->assertEquals(
            $this->scoringService
                ->setClient($client)
                ->getEmailScoring(),
            $excepted
        );
    }

    public function emailsProvider(): array
    {
        return [
            'google' => ['test@gmail.com', 10],
            'yandex' => ['test@yandex.ru', 8],
            'mail.ru' => ['test@mail.ru', 6],
            'Прочее' => ['test@other.ru', 3]
        ];
    }


    /**
     * @dataProvider educationsProvider
     */
    public function testEducations($education, $excepted)
    {
        $client = new Client();
        $client->setEducation($education);

        $this->assertEquals(
            $this->scoringService
                ->setClient($client)
                ->getEducationScoring(),
            $excepted
        );
    }

    public function educationsProvider(): array
    {
        $container = self::getContainer();
        $entityManager = $container
            ->get('doctrine')
            ->getManager();

        $educations = $entityManager->getRepository(Education::class)->findAll();

        $provider = [];
        foreach ($educations as $education) {
            $provider[$education->getName()] = [$education, $education->getScoringValue()];

        }
        return $provider;
    }

    /**
     * @dataProvider processDataProvider
     */
    public function testProcessData($process, $excepted)
    {
        $client = new Client();
        $client->setProcessingConsent($process);

        $this->assertEquals($this->scoringService->setClient($client)->getProcessingScoring(), $excepted);
    }

    public function processDataProvider(): array
    {
        return [
            'Есть разрешение' => [true, 4],
            'Нет разрешения' => [false, 0],
            ];

    }

}