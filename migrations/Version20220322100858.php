<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220322100858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD education_id INT DEFAULT NULL, DROP education');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404552CA1BD71 FOREIGN KEY (education_id) REFERENCES education (id)');
        $this->addSql('CREATE INDEX IDX_C74404552CA1BD71 ON client (education_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404552CA1BD71');
        $this->addSql('DROP INDEX IDX_C74404552CA1BD71 ON client');
        $this->addSql('ALTER TABLE client ADD education SMALLINT NOT NULL, DROP education_id');
    }
}
